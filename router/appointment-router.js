/**
 * Created by bidau on 02/06/2016.
 */
var router = require("express").Router();
var mysql = require("../config/database");
var positions = require("../utility/location");
var notification = require("../utility/notification");

var allAppointment = function(req, res){
    var id = req.params.id;
    mysql.findAll("select * from mission WHERE (id_user=? or id_pro=?) and date_mission>=curdate() and  state_mission!=2 order by date_mission", [id, id]).then(function(user){
        res.json(user);
    });
};



router.get("/allAppointment/:id", allAppointment);

var getHistorique = function(req, res){
  var id = req.params.id;
    mysql.findAll("select * from mission WHERE (id_user=? or id_pro=?) and (date_mission<curdate() or  state_mission=2 )order by date_mission DESC", [id, id]).then(function(user){
        res.json(user);
    });
};

router.get("/historique/:id", getHistorique);


/**
 * 
 * @param req
 * @param res
 */
var insert = function(req, res){
    var id_user = req.body.id_user;
    var id_address = req.body.id_address;
    var taskType = req.body.taskType;
    var detail = req.body.detail;
    var date = req.body.date;
    var paymentMethod = req.body.paymentMethod;
    var time_period = req.body.time_period;
    var repeat = req.body.repeat;

    console.log(date);
    mysql.insert("insert into mission(id_user, id_address, taskType, detail, paymentMethod, date_mission, time_period, repeat_type,  state_mission) VALUES(?,?,?,?,?,?,?, ?, ?)", [id_user, id_address, taskType, detail, paymentMethod, date, time_period, repeat, 0]).then(function(task){
        res.send(true);
    }, function(data){
        res.send(false);
    });

};

router.post("/create", insert);


var cancel = function(req, res){
    var id = req.body.id;
    var date = req.body.date;
    var state = req.body.state;
    var detail = req.body.detail;
    console.log(date);
    if(date!= "") {
        mysql.update("update mission set detail=?, date_mission=?,  state_mission=? where id=?", [detail, date, state, id]).then(function(task){
            mysql.findAll("SELECT * from notification where id_user=(SELECT id_pro from mission where id=?);", id).then(function(data){
                console.log(data);
                data.forEach(function(notif, i){
                    //console.log("id:"+id);
                    console.log(notif);
                    notification.sendNotification(notif.token, "Mission modiffier", "Une mission à été modifié par votre client");
                })
            }, function(err){
                console.log(err);
            });
           res.send(true);
        }, function(){
            res.send(false);
        });
    }else{
        console.log("ok");
        mysql.update("update mission set detail=?,  state_mission=? where id=?", [detail, state, id]).then(function(task){
            mysql.findAll("SELECT * from notification where id_user=(SELECT id_pro from mission where id=?);", id).then(function(data){
                console.log(data);
                data.forEach(function(notif, i){
                    //console.log("id:"+id);
                    console.log(notif);
                    notification.sendNotification(notif.token, "Mission annulé", "Une mission à été annulé par votre client");
                })
            }, function(err){
                console.log(err);
            });
            res.send(true);
        }, function(){
            res.send(false);
        });
    }
};
router.post("/cancel", cancel);

/**
 * 
 * @param req
 * @param res
 */
var searchFromAddress = function(req, res){
    console.log("ok");
    var address = req.params.add;
    var type = req.params.type;
    positions.getPosition(address, function(err, resPos){
        var position =resPos[0];
        var latitude = position.latitude;
        var longitude = position.longitude;
        var ids = [];
        mysql.findAll("SELECT id, lattitude, longitude, SQRT(POW(69.1 * (lattitude - ?), 2) + POW(69.1 * (? - longitude) * COS(lattitude / 57.3), 2)) AS distance FROM address HAVING distance < 25 ORDER BY distance;", [latitude, longitude]).then(function(add){
            add.forEach(function(address, i){
                ids.push(address.id);
            });
            mysql.findAll("SELECT * FROM mission where id_address in (?) and  state_mission=0 and date_mission>=curdate() and taskType=?", [ids, type]).then(function(response){
               res.send(response);
            }, function(err){
                console.log(err);
            });
        }, function(err){
            console.log(err);
        })
    })
};


router.get("/searchFromAdd/:add/:type", searchFromAddress);

var searchFromPosition = function(req, res){
    var latitude = req.params.latitude;
    var longitude = req.params.longitude;
    var type = req.params.type;
    var ids = [];
    mysql.findAll("SELECT id, lattitude, longitude, SQRT(POW(69.1 * (lattitude - ?), 2) + POW(69.1 * (? - longitude) * COS(lattitude / 57.3), 2)) AS distance FROM address HAVING distance < 25 ORDER BY distance;", [latitude, longitude]).then(function(add){
        add.forEach(function(address, i){
            ids.push(address.id);
        });
        mysql.findAll("SELECT * FROM mission where id_address in (?) and  state_mission=0 and date_mission>=curdate() and taskType=?", [ids, type]).then(function(response){
            res.send(response);
        }, function(err){
            console.log(err);
        });
    }, function(err){
        console.log(err);
    })
};

router.get("/searchFromPosition/:latitude/:longitude/:type", searchFromPosition);


var accepted = function(req, res){
    var id = req.body.id;
    var id_task = req.body.id_task;
    console.log(id, id_task);
    mysql.update("UPDATE mission set id_pro=? , state_mission=4 where id=?", [id, id_task]).then(function(data){
        console.log(id);
        mysql.findAll("SELECT * from notification where id_user=(SELECT id_user from mission where id=?);", id_task).then(function(data){
            console.log(data);
            data.forEach(function(notif, i){
                //console.log("id:"+id);
                console.log(notif);
                notification.sendNotification(notif.token, "Mission accepter", "Un professionnel à accepté votre mission");
            })
        }, function(err){
            console.log(err);
        });
        res.send(true);
    }, function(err){
        console.log(err);
        res.send(false);
    });
};
router.post("/accepted", accepted);


var refuse = function(req, res){
    var id = req.body.id;
    mysql.update("UPDATE mission set id_pro=null where id=?", id).then(function(data){
        res.send(true);
    }, function(err){
        console.log(err);
    });
};
router.post("/refuse", refuse);
module.exports = router;