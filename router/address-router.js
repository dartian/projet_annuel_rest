/**
 * Created by d.bidaud on 15/06/2016.
 */
var router = require("express").Router();
var mysql = require("../config/database");
var position = require("../utility/location");

var getDefaultAddress = function (req, res) {
    var id = req.params.id;
    console.log(id);
    console.log("ok");
    mysql.findOne("select * from address WHERE id_user=? and default_address=1", id).then(function(address){
        console.log(address);
        res.json(address);
    });
};



router.get("/getDefaultAddress/:id", getDefaultAddress);


var getAddress = function(req, res){
    var id = req.params.id;
    console.log(id);
    console.log("ok");
    mysql.findOne("select * from address WHERE id=? ;", id).then(function(address){
        console.log(address);
        res.json(address);
    });
};
router.get("/getAddress/:id", getAddress);

var create = function(req, res){
    var zipCode = req.body.zipCode;
    var city = req.body.city;
    var address = req.body.address;
    var id_user = req.body.id_user;
    position.getPosition(address+" "+zipCode+" "+city , function(err, addInfo){
        var response =addInfo[0];
        console.log(response);
        var longitude = response.longitude;
        var lattitude =response.latitude;
        mysql.insert("INSERT INTO address(zipCode, city, address, id_user, default_address, longitude, lattitude) VALUES(?, ?, ?, ?, 0, ?, ?)", [zipCode, city, address, id_user, longitude, lattitude]).then(function(id){
            console.log(id);
            res.json(id);
        }, function(response){
            res.sendStatus(502);
        })    
    });
    
};

router.post("/create", create);
module.exports = router;