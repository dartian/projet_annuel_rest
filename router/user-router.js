/**
 * Created by bidau on 24/05/2016.
 */
var router = require("express").Router();
var mysql = require("../config/database");
var positions = require("../utility/location");
var crypto = require("crypto");
var notif = require("../utility/notification");


var account = function(req, res){
    var id = req.params.id;
    mysql.findOne("select * from users WHERE id=?", id).then(function(user){
        res.json(user);
    });

};



router.get("/account/:id", account);




var authenticate = function(req, res){
    var username = req.params.username;
    var password = req.params.password;
    var sha1 = crypto.createHash("sha1");
    sha1.update(password);
    var hashpass = sha1.digest("hex");
    mysql.findOne("select * from users WHERE eMail=? and password=?", [username, hashpass]).then(function(user){
        if(user!= null){
            res.json(user);
        }else{
            res.sendStatus(404);
        }

    })
};

router.get("/authenticate/:username/:password", authenticate);



var signIn = function(req, res){
    var first_name  = req.body.first_name;
    var last_name = req.body.last_name;
    var phoneNumber = req.body.phoneNumber;
    var eMail = req.body.eMail;
    var type = req.body.type;
    var birthDate = req.body.birthDate;
    var password = req.body.password;
    var address = req.body.address;
    var zipCode = req.body.zipCode;
    var city = req.body.city;
    var civility = req.body.civility;
    var siret = req.body.siret;
    var sha1 = crypto.createHash("sha1");
    sha1.update(password);
    var hashpass = sha1.digest("hex");
    //console.log(first_name, last_name, phoneNumber, eMail, type, birthDate, password, address, zipCode, city, civility);
    mysql.findOne("SELECT * from users where eMail=?", eMail).then(function(user){
        console.log(user);
        if(user==null){
            mysql.insert("INSERT INTO users(first_name,last_name,phoneNumber,eMail,type,birthDate,password, civilite, siret) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", [first_name, last_name, phoneNumber, eMail, type, birthDate, hashpass, civility, siret]).then(function(user){
                console.log(address+""+city);

                positions.getPosition(address+""+city, function (err, resp) {
                    console.log(resp);
                    if(resp!=null) {
                        var response = resp[0];
                        var latitude = response.latitude;
                        var longitude = response.longitude;
                        console.log("ok");
                        mysql.insert("INSERT INTO address(zipCode,city,address,id_user,default_address, longitude, lattitude) VALUES(?, ?, ?, ?, ?, ?, ?)", [zipCode, city, address, user, 1, longitude, latitude]).then(function (address) {
                            res.send(true);
                        }, function (eerro) {
                            res.send(false);
                        });
                    }
                });

            }, function(data){
                console.log(data);
            })
        }else{
            console.log("exist");
            res.send(false);
        }
    },function(err){
        res.send(false);
    });

};

router.post("/signIn", signIn);

var update = function(req, res){
    var id = req.body.id;
    var first_name  = req.body.first_name;
    var last_name = req.body.last_name;
    var phoneNumber = req.body.phoneNumber;
    var eMail = req.body.eMail;
    var address = req.body.address;
    var zipCode = req.body.zipCode;
    var city = req.body.city;
    

    mysql.update("UPDATE users set first_name=?, last_name=?, phoneNumber=?, eMail=? where id=?",
        [first_name, last_name, phoneNumber, eMail, id]).then(function(data){
        mysql.update("UPDATE address SET zipCode=?, address=?, city=? WHERE id_user=? AND default_address=1",
            [zipCode, address, city, id]).then(function(data){
            res.send(true);
        }, function(response){
            res.send(false);
        })
    })
};

router.post("/update", update);


var cryptePass = function(req, res){
    var password = req.params.password;
    var sha1 = crypto.createHash("sha1");
    sha1.update(password);
    //var hashpass = sha1.digest(password);
    console.log(sha1.digest("hex"));
};
router.get("/cryptPass/:password", cryptePass);



var insertNotification = function(req, res){
    var id = req.body.id;
    console.log(id);
    var token = req.body.token;

    
    mysql.insert("INSERT notification (id_user, token) VALUES(?,?)", [id, token]).then(function(data){
        res.send(true);
    }, function(err){
        res.send(false);
        console.log(err);
    })
};
router.post("/notifiedUpdate", insertNotification);
module.exports = router;