-- phpMyAdmin SQL Dump
-- version 4.4.13.1
-- http://www.phpmyadmin.net
--
-- Client :  dailyhommfsj.mysql.db
-- Généré le :  Ven 27 Mai 2016 à 09:26
-- Version du serveur :  5.5.46-0+deb7u1-log
-- Version de PHP :  5.4.45-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dailyhommfsj`
--

-- create table user
-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `users`(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  civilite int
  first_name varchar(50),
  last_name varchar(50),
  phoneNumber varchar(10),
  eMail varchar(100) USER,
  type int,
  birthDate DATE,
  password varchar(100),
  id_notification varchar(50),
  notified int,
  siret varchar(100)
);
--
-- Structure de la table `wp_ab_appointments`
--

-- create table address

CREATE TABLE IF NOT EXISTS `address`(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  zipCode varchar(10),
  city varchar(50),
  country varchar(50),
  address varchar(100),
  id_user int,
  default_address int,
  longitude float,
  lattitude float
);

--48.9051844,2.2741298,

ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- create table mission

CREATE TABLE IF NOT EXISTS `mission`(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  id_user int,
  id_address int,
  id_pro int,
  taskType int,
  detail text,
  date_mission DATETIME,
  paymentMethod int,
  state int,
  time_period int,
  repeat_type int,
  state_mission int
);

ALTER TABLE `mission`
  ADD CONSTRAINT `mission_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mission_ibfk_2` FOREIGN KEY (`id_pro`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mission_ibfk_3` FOREIGN KEY (`id_address`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `notification`(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    id_user int,
    token varchar(250) UNIQUE
);

ALTER TABLE `notification`
    ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
