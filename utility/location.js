/**
 * Created by bidau on 19/06/2016.
 */
var NodeGeocoder = require("node-geocoder");

var options = {
    provider: 'google',

    // Optional depending on the providers
    httpAdapter: 'https', // Default
    //apiKey: 'YOUR_API_KEY', // for Mapquest, OpenCage, Google Premier
    formatter: null
};

var geocoder  = NodeGeocoder(options);

module.exports.getPosition = function(address, callback){
    geocoder.geocode(address, callback);
};