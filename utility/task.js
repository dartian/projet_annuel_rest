/**
 * Created by bidau on 26/06/2016.
 */
var cron = require("node-cron");
var mysql = require("../config/database");

module.exports.endTask = cron.schedule('10 * * * *', function () {
    console.log("run every 10 minutes");
    mysql.findAll("select * from mission WHERE date_mission<curdate() and  state_mission!=2").then(function(tasks){
        tasks.forEach(function(task, i){
            if(task.id_pro==null){
                mysql.update("update mission set  state_mission=2 where id=?", task.id).then(function(response){
                    console.log("cancel");
                });
            }else{
                mysql.update("update mission set  state_mission=3 where id=?", task.id).then(function(response){
                    console.log("finish");
                });
            }
        })
    });
}, false
);  