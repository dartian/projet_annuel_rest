/**
 * Created by bidau on 24/05/2016.
 */
var express = require("express");

var app = express();
var userRouter = require("./router/user-router");
var appointmentRouter = require("./router/appointment-router");
var addressRouter = require("./router/address-router");
var bodyParser = require('body-parser');
var notif = require("./utility/notification");
var task = require("./utility/task");


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(express.static("/var/www/html"));



app.get("/", function(req, res){
   res.sendFile("index.html");
});

app.listen(80, function(){
   console.log("connection ok");
});


task.endTask.start();

app.use("/user", userRouter);
app.use("/appointment", appointmentRouter);
app.use("/address", addressRouter);