#! /bin/bash

if [ "$#" -ne 1 ]; then
    echo "Missing argument \n"
    echo "put start for start the server"
    echo "put restart for restart the server"
    echo "put stop for stop the server"

elif [ $1 = "start" ]; then
    echo "start"
    node server.js &
    echo "\n"
elif [ $1 = "stop" ]; then
     kill `ps -ef | grep node`
    echo "stop"
elif [ $1 = "restart" ]; then
    echo "stop"
    kill `ps -ef | grep node`
    echo "restart"
    node server.js &
    echo "restart"
fi
